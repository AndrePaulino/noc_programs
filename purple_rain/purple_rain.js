let w,d;
let drops = [];
let dropsCount = 1200;

function setup() {
    w = windowWidth;
    d = windowHeight;
    createCanvas(w, d);
    for (let i = 0; i <= dropsCount; i++) {
        drops[i] = new Drop(50, 10, 3);
    }
}

function draw() {
    background(230, 230, 255);

    drops.forEach(drop => {
        drop.fall();
        drop.render();
    });
}

class Drop {
    constructor(maxLength, maxSpeed, maxThick) {
        this.maxLength = maxLength;
        this.maxSpeed = maxSpeed;
        this.maxThick = maxThick;

        this.minDepth = 5;
        this.maxDepth = 20;

        this.x = random(width);
        this.y = random(-650, -50);
        this.z = random(this.minDepth, this.maxDepth);

        this.len = map(this.z, this.minDepth, this.maxDepth, 5, this.maxLength);
        this.ySpeed = map(this.z, this.minDepth, this.maxDepth, 1, this.maxSpeed);
    }

    fall() {
        let grav = map(this.z, this.minDepth, this.maxDepth, 0, 0.2);

        this.y += this.ySpeed
        this.ySpeed += grav;

        if (this.y > height) {
            this.x = random(width);
            this.y = random(-1000, -51);
            this.z = random(this.minDepth, this.maxDepth);

            this.ySpeed = map(this.z, 0, this.maxDepth, 1, this.maxSpeed);

        }
    }

    render() {
        let thick = map(this.z, this.minDepth, this.maxDepth, 0, this.maxThick);

        strokeWeight(thick);
        stroke(138, 43, 226);
        line(this.x, this.y, this.x, this.y + this.len);
    }
}
