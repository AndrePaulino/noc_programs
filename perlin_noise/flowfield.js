let xoff, yoff, zoff;
let cols, rows;
let fr, p;
let particles, flowfield;

//** Settings
let particlesCount = 700
let inc = 0.0915;
let scale = 15;
let wind = 0,
    windSpeed = 0.000195;

function setup() {
    // let c = createCanvas(1850, 900);
    let c = createCanvas(800, 800);
    noiseDetail(4, 0.525);
    background(60);
    colorMode(HSB, 255)

    p = createP('Mova o mouse lateralmente para alterar a cor e verticalmente a transparência');
    fr = createP();
    cols = floor(width / scale);
    rows = floor(height / scale);

    flowfield = new Array(cols * rows);
    particles = [];
    for (let i = 0; i < particlesCount; i++) {
        particles[i] = new Particle();
    }
}

function draw() {
    wind += windSpeed;
    zoff = wind;

    yoff = 0;
    for (let y = 0; y < rows; y++) {
        xoff = 0;
        for (let x = 0; x < cols; x++) {
            let index = (x + y * cols);

            angle = map(noise(xoff, yoff, zoff), 0, 1, 0, TWO_PI * 4);

            let v = p5.Vector.fromAngle(angle);
            v.setMag(1);
            flowfield[index] = v;

            xoff += inc;
        }
        yoff += inc;
    }

    particles.forEach(particle => {
        particle.followFlowfield(flowfield);
        particle.update();
        particle.border();
        particle.render();
    });

    fr.html(floor(frameRate()));
}
