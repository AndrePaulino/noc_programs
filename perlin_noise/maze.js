let xoff1;
let xoff2;
let inc;
let scale;

function setup() {
    let c = createCanvas(1080, 1080);
    frameRate(30);
    noiseDetail(4, 0.5);
    inc = 0.00021;
    scale = 1.75;
}

function draw() {
    background(255);
    noFill();
    stroke(10);
    strokeWeight(1);
    beginShape();

    xoff1 = 0;
    for (let x = 0; x < width/scale; x++) {
        xoff2 = 0;
        b = map(noise(xoff2, xoff1), 0, 1, 0, height);
        for (let y = 0; y < height/scale; y++) {
            a = map(noise(xoff1,xoff2), 0, 1, 0, width);

            vertex(a,b);

            xoff1 += inc;
        }
        xoff2 += inc;
    }

    endShape();
    noLoop();
}

