function setup() {
    createCanvas(500, 500);
    colorMode(HSB, 100);
    noFill();
}

let res = 100;
let amp = 6;
let iter = 50;
let xOffset = 0;
let yOffset = 0;
let speed = 0.001;
let time = 0;

function draw() {
    background(0, 0, 10);
    translate(width / 2, height / 2);
    time += speed;
    xOffset = sin(time * 0.05) * 100 + 5000;
    yOffset = cos(time * 0.05) * 100 + 5000;

    for (let j = 0; j < iter; j++) {
        beginShape();
        for (let i = 0; i < res; i++) {
            let n = noise(xOffset + sin(i * TWO_PI / res) * 0.01 * j, yOffset + cos(i * TWO_PI / res) * 0.01 * j, time);

            strokeWeight(j / iter);
            stroke(0, 0, 100);
            let x = sin(i * TWO_PI / res) * j * amp * n + 1;
            let y = cos(i * TWO_PI / res) * j * amp * n + 1;
            vertex(x, y);
        }
        endShape(CLOSE);
    }
}
