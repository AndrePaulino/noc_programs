function Particle(size) {
    this.pos = createVector(random(width), random(height));
    this.vel = createVector(0, 0);
    this.acc = createVector(0, 0);
    this.maxSpeed = 6.5;
    this.prevPos = this.pos.copy();
    this.hue = random(255);
    this.alpha = random(25, 55);

    this.update = function () {
        this.vel.add(this.acc);
        this.vel.limit(this.maxSpeed);

        this.pos.add(this.vel);
        this.acc.mult(0);
    }

    this.render = function () {
        // this.hue = map(mouseX,0,width,0,255)
        // this.alpha = map(mouseY,0,height,0,255)

        strokeCap(SQUARE);
        stroke(this.hue, 255, 255, this.alpha);

        if (this.hue > 255) this.hue = 0;
        this.hue++;

        strokeWeight(0.5);
        line(this.pos.x, this.pos.y, this.prevPos.x, this.prevPos.y);

        this.updatePrevPos();
    }

    this.followFlowfield = function (flowfield) {
        let index = floor(this.pos.x / scale) + (floor(this.pos.y / scale) * cols);

        let force = flowfield[index];
        this.applyForce(force);
    }

    this.applyForce = function (force) {
        this.acc.add(force);
    }

    this.updatePrevPos = function () {
        this.prevPos.x = this.pos.x;
        this.prevPos.y = this.pos.y;
    }

    this.border = function () {
        if (this.pos.x > width + size / 2) {
            this.pos.x = 0 + size / 2;
            this.updatePrevPos();
        }
        if (this.pos.x < 0 - size / 2) {
            this.pos.x = width - size / 2;
            this.updatePrevPos();
        }
        if (this.pos.y > height + size / 2) {
            this.pos.y = 0 + size / 2;
            this.updatePrevPos();
        }
        if (this.pos.y < 0 - size / 2) {
            this.pos.y = height - size / 2;
            this.updatePrevPos();
        }
    }

}
