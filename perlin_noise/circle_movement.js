let xoff1;
let xoff2;
let inc;
let x;

function setup() {
    createCanvas(900, 900);
    xoff1 = 1;
    xoff2 = 5000;
    inc = 0.025;
}

function draw() {
    background(255);

    // map perlin values in same plane, different points
    y = map(noise(xoff2), 0, 1, 0, height);
    x = map(noise(xoff1), 0, 1, 0, width);

    // move through the perlin plane
    xoff1 += inc;
    xoff2 += inc;

    // render circle with movement
    noStroke();
    fill(20);
    ellipse(x, y, 6, 6);
}
