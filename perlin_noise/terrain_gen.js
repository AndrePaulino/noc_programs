let xoff;
let yoff;
let inc = 0.055;
let yoffStart = 0.0225;
let scl = 15;
let w = 2200;
let h = 2200;
let yoffSpeed = 0;
let terrain = [];
let color = [];

function setup() {
    createCanvas(800, 800, WEBGL);
    noiseDetail(5, 0.55);

    cols = w / scl;
    rows = h / scl;

    for (let x = 0; x < cols; x++) {
        terrain[x] = [];
        color[x] = [];
        for (let y = 0; y < rows; y++) {
            terrain[x][y] = 0;
            color[x][y] = 0;
        }
    }
}

function draw() {
    yoffSpeed -= yoffStart;
    yoff = yoffSpeed;
    for (let y = 0; y < rows; y++) {
        xoff = 0;
        for (let x = 0; x < cols; x++) {
            let r = noise(xoff, yoff);

            terrain[x][y] = map(r, 0, 1, -90, 90);
            color[x][y] = (r * 255);

            xoff += inc;
        }
        yoff += inc;
    }

    background(0);
    noStroke();

    // move the plane for 3d view
    translate(0, 50);
    rotateX(PI / 3);
    translate(-w / 2, -h / 2);

    for (let y = 0; y < rows - 1; y++) {

        beginShape(TRIANGLE_STRIP);
        for (let x = 0; x < cols; x++) {
            let r = noise(xoff, yoff);

            fill(color[x][y]);

            vertex(x * scl, y * scl, terrain[x][y]);
            vertex(x * scl, (y + 1) * scl, terrain[x][y + 1]);
        }
        endShape();
    }
    // noLoop();
}
