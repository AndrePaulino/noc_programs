let xoff;
let yoff;
let inc;

function setup() {
    let c = createCanvas(800, 800);
    inc = 0.0075;
    noiseDetail(5, 0.625);
}

function draw() {
    background(255);

    loadPixels();
    yoff = 0;
    for (let y = 0; y < height; y++) {
        xoff = 0;
        for (let x = 0; x < width; x++) {
            let index = (x + y * width) * 4;
            let r = map(noise(xoff, yoff), 0, 1, 0, 255);

            pixels[index + 0] = 255;
            pixels[index + 1] = r;
            pixels[index + 2] = r;
            pixels[index + 3] = 255;
            xoff += inc;
        }
        yoff += inc;
    }
    updatePixels();
    noLoop();
}
