//took help from The coding train https://www.youtube.com/watch?v=ZI1dmHv3MeM&t=267s

var rMax = 0.8; // the maximum motion difference in the vertices of one circle
var move = 0; // the variable will put the motion in the blob
var zOff = 0; // experimental offset for the noise value
var inc = 1;

function setup() {
    createCanvas(windowWidth, windowHeight);
    background(0);
    //blendMode(MULTIPLY);
}

function draw() {
    //background(0);
    blendMode(ADD);
    translate(windowWidth / 2, windowHeight / 2);
    //noStroke();
    // fill(200,0,0);//try using fill
    noFill(); // try using no fill and stroke instead

    stroke(255);
    //beginShape();
    xPoint = 0;
    yPoint = 0;

    blob(xPoint, yPoint);

    //endShape();
}

function blob(xPoint, yPoint) {
    var x1 = xPoint;
    var y1 = yPoint;

    beginShape();
    strokeWeight(0.1);
    for (var i = 0; i < TWO_PI; i += 0.2) { // i increment defines the number of vertices/sphere in the circle
        //var r= 100;            // defines the diameter/path of the vertices/small sphere in the circle
        var smooth = 100;
        //var xOffset= cos(i)+1; // Polar cartician
        //var yOffset= sin(i)+1; // adding one so that the value comes in +ve because later when used in perlin noise it is in +ve(perlin noise is always a +ve value)
        var xOffset = map(cos(i), -1, 1, 0, rMax); //here instead of adding a positive value I am mapping positive values between 10 and rMAx global variable
        var yOffset = map(sin(i), -1, 1, 0, rMax);
        //var r=noise(frameCount/smooth)*200;  //here diameter/path of vertices/small sphere in the circle is getting perlin noise value via frameCount which is volatile
        //values which means it will very fast and too random and to control it I have divided it with a smooth value here and
        //as the value is b/w (0)-1 I have later multiplied it with 200 to make it a bigger value.
        var r = map(noise(xOffset, yOffset, zOff), 0, 1, 0, smooth) * inc; // here the value is coming with respect to the xoffset and yoffset value and will NOT have a
        // gap difference in the perfect circle and will perfectly end at the start point map suntax is helping in
        // assigning values between 20(min value) to (smooth) rather than 0 to 1.

        x = x1 + r * cos(i) + 2; // Polar cartician coordinate (the cos and sin representation of x and y coordinates)
        y = y1 + r * sin(i) + 2;
        curveVertex(x, y);

        //move+=0.0001; // Change the increment value to see different pattern.
        //ellipse(x,y,4,4);
    }
    endShape(CLOSE);
    zOff += 0.003;
    inc += 0.002;
    if (inc >= 10) {
        noLoop()
    }
}
