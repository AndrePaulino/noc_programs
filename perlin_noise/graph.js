let start;
let xoff;
let inc;
let speed;

function setup() {
    let c = createCanvas(900, 900);
    noiseDetail(4.5, 0.625);
    start = 0;
    inc = 0.0075;
    xoff = start;
    speed = 0.025
}

function draw() {
    background(245);
    xoff = start;

    // render perlin noise plane
    noFill();
    stroke(35);
    beginShape();
    for (let x = 0; x < width; x++) {
        y = map(noise(xoff), 0, 1, 0, height); // pick next y
        vertex(x, y); // draw the vertex
        xoff += inc; // move through the noise plane
    }
    endShape();
    // advance on the plane to keep drawing
    start += speed;
}
