let radius = 150;
let xoff = yoff = zoff = 0;
let xinc = 0.075, yinc = 0.125, zinc = 0.0925;
let x, y;
let noiseScl = 150;

function setup() {
    createCanvas(600, 600);
    // frameRate(30);
}

function draw() {
    background(255);
    stroke(0);
    translate(200, 200)

    push();
    beginShape();

    for (let i = 0; i < TWO_PI; i += 0.01) {
        offset = map(noise(xoff, yoff,zoff), 0, 1, -25, 25);
        let r = radius;
        x = r * cos(i);
        y = r * sin(i);

        let newX = x + (noise((xoff + x) / noiseScl, (yoff + y) / noiseScl, zoff) * cos(i) * 25);
        let newY = y + (noise((xoff + x) / noiseScl, (yoff + y) / noiseScl, zoff) * sin(i) * 25);

        vertex(newX, newY);
    }

    endShape();
    pop();

    xoff += xinc;
    yoff += yinc;
    zoff += zinc;
}
