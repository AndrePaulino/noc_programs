function setup() {
    createCanvas(450, 450);
}

function draw() {
    background(51);
    translate(width / 2, height / 2);

    let mouse = createVector(mouseX, mouseY);
    let center = createVector(width / 2, height / 2);
    mouse.sub(center);

    strokeWeight(2);
    stroke(255);
    line(0, 0, mouse.x, mouse.y);
}
