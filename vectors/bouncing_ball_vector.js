let position;
let velocity;
let size;
let frameSize;
let border;

function setup() {
    createCanvas(450, 900);
    position = createVector(width / 2, height / 2);
    velocity = createVector(1.75, 3.63);
    size = 50;
    frameSize = 20;
    border = (size + frameSize) / 2;
}

function draw() {
    // Render frame
    strokeWeight(frameSize);
    stroke(20);
    fill(245);
    rect(0, 0, width, height);

    // Moving ball
    position.add(velocity);

    if (position.x < 0 + border) {
        velocity.x = random(1, 5);
    } else if (position.x > width - border) {
        velocity.x = random(1, 5) * -1;
    }
    if (position.y < 0 + border) {
        velocity.y = random(1, 5);
    } else if (position.y > height - border) {
        velocity.y = random(1, 5) * -1;
    }
    // Render ball
    c = color(125, 109, 189);
    fill(c);
    noStroke();
    ellipse(position.x, position.y, size);
}
