let xSpeed;
let ySpeed;
let x;
let y;
let size;

function setup() {
    createCanvas(450, 900);
    x = width / 2;
    y = height / 2;
    xSpeed = 1.75;
    ySpeed = 3.63;
    size = 50;
}

function draw() {
    background(245);


    // Moving ball
    x += xSpeed;
    y += ySpeed;

    if (x < 0 + size/2) {
        xSpeed = random(1, 10);
    } else if (x > width - size / 2) {
        xSpeed = random(1, 10) * -1;
    }
    if (y < 0 + size/2) {
        ySpeed = random(1, 10);
    } else if (y > height - size/2) {
        ySpeed = random(1, 10) * -1;
    }

    // Render ball
    c = color(125, 109, 189);
    fill(c);
    noStroke();
    ellipse(x, y, size);
}
