class Cube {
    constructor(_translateX = 0, _translateY = 0, _translateZ = 0, _d) {
        this.pos = createVector(_translateX, _translateY, _translateZ);
        this.d = _d;
    }

    render() {
        push();
        noFill();
        stroke(0)
        translate(this.pos.x, this.pos.y, this.pos.z);
        box(this.d);
        pop();
    }

    split() {
        let cubes = [];
        let size = this.d / 3;

        for (let i = -1; i < 2; i++) {
            for (let j = -1; j < 2; j++) {
                for (let k = -1; k < 2; k++) {
                    if (abs(i) + abs(j) + abs(k) > 2) {
                        cubes.push(new Cube(
                            this.pos.x + i * size,
                            this.pos.y + j * size,
                            this.pos.z + k * size,
                            size
                            ));
                    }
                }
            }
        }
        return cubes;
    }
}
