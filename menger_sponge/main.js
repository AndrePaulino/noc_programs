var cubes = [];
let a = 0;

function setup() {
    createCanvas(600, 600, WEBGL);
    cubes.push(new Cube(0, 0, 0, 200));
}

function draw() {
    background(220);

    rotateX(frameCount * 0.00375)
    rotateY(frameCount * 0.00485)
    rotateZ(frameCount * 0.00195)

    cubes.forEach(cube => {
        cube.render();
    });

    a += 0.01;
}

function mousePressed() {
    let next = [];

    cubes.forEach(cube => {
        next = next.concat(cube.split());
    });
    cubes = next;
}
