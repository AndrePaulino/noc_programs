function setup() {
  createCanvas(400, 400);
}

function draw() {
    background(0)
    openSimplex = SimplexNoise(random(42))
    let x, y, index = 0
    for (y = 0; y < height; y++) {
        for (x = 0; x < width; x++) {
            const value = (openSimplex.noise2D(x / noiseScale, y / noiseScale) + 0.75) * 128
            stroke(value, 0, 0)
            point(x, y)
        }
    }
}
