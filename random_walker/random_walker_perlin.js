let walker;
let inc = 0.0125;

function setup() {
    createCanvas(450, 900);
    background(55);
    walker = new RandomWalker(6, 6);
}

function draw() {
    walker.walk();
    walker.render();
}
class RandomWalker {
    constructor(size, step) {
        this.size = size;
        this.step = step;
        this.pos = createVector(width / 2, height / 2);
        this.off = createVector(0, 5000);
    }
    render() {
        noStroke();
        let c = pickColor();
        fill(c);
        // rect(this.pos.x, this.pos.y, this.size);
        ellipse(this.pos.x, this.pos.y, this.size);

        function pickColor() {
            let h = random(190, 230);
            let s = random(0, 45);
            let l = random(190, 230);
            let a = random(60, 150);
            let hsl = color(h, s, l, a);
            return hsl;
        }
    }
    walk() {
        this.pos.x = map(noise(this.off.x), 0, 1, 0, width);
        this.pos.y = map(noise(this.off.y), 0, 1, 0, height);
        this.off.add(inc, inc);
    }
}
