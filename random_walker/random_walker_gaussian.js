let walker;
var directions = ['left', 'stay', 'down-right', 'top', 'down-left', 'right', 'top-right', 'down', 'top-left'];

function setup() {
    createCanvas(450, 900);
    background(245);
    // frameRate(30);
    colorMode(HSB);
    walker = new RandomWalker(15, 20);
}

function draw() {
    walker.render();
    walker.walk();
}
class RandomWalker {
    constructor(size, step) {
        this.size = size;
        this.step = step;
        this.x = width / 2;
        this.y = height / 2;
    }
    render() {
        noStroke();
        let c = pickColor();
        fill(c);
        // rect(this.x, this.y, this.size);
        ellipse(this.x, this.y, this.size, this.size);

        function pickColor() {
            let h = random(285, 325);
            let s = random(65, 80);
            let l = random(45, 60);
            let a = 0.25;
            let hsla = color(h, s, l, a);
            return hsla;
        }
    }
    walk() {
        let direction = directions[floor((randomGaussian() * directions.length-2) + (directions.length / 2))];
        switch (direction) {
            case 'stay':
                break;
            case 'top':
                this.x += 0;
                this.y = this.yConstrain(this.y - this.step);
                break;
            case 'right':
                this.x = this.xConstrain(this.x + this.step);
                this.y += 0;
                break;
            case 'down':
                this.x += 0;
                this.y = this.yConstrain(this.y + this.step);
                break;
            case 'left':
                this.x = this.xConstrain(this.x - this.step);
                this.y += 0;
                break;
            case 'top-right':
                this.x = this.xConstrain(this.x + this.step);
                this.y = this.yConstrain(this.y - this.step);
                break;
            case 'down-right':
                this.x = this.xConstrain(this.x + this.step);
                this.y = this.yConstrain(this.y + this.step);
                break;
            case 'down-left':
                this.x = this.xConstrain(this.x - this.step);
                this.y = this.yConstrain(this.y + this.step);
                break;
            case 'top-left':
                this.x = this.xConstrain(this.x - this.step);
                this.y = this.yConstrain(this.y - this.step);
                break;
        }
    }
    xConstrain(num) {
        const xMIN = 0 + this.size;
        const xMAX = width - this.size;
        return Math.min(Math.max(num, xMIN), xMAX);
    }
    yConstrain(num) {
        const yMIN = 0 + this.size;
        const yMAX = height - this.size;
        return Math.min(Math.max(num, yMIN), yMAX);
    }
}
