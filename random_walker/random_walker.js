let walker;
var directions = ['stay', 'top', 'right', 'down', 'left', 'top-right', 'down-right', 'down-left', 'top-left'];

function setup() {
    createCanvas(450, 900);
    background(245);
    walker = new RandomWalker(5, 5);
}
function draw() {
    walker.render();
    walker.walk();
}
class RandomWalker {
    constructor(size, step) {
        this.size = size;
        this.step = step;
        this.x = width / 2;
        this.y = height / 2;
    }
    render() {
        noStroke();
        colorMode(HSB);
        let c = pickColor();
        fill(c);
        rect(this.x, this.y, this.size);
        // ellipse(this.x, this.y, this.size, this.size);
        function pickColor() {
            let h = random(240, 360);
            let s = random(25, 75);
            let l = random(35, 85);
            let hsl = color(h, s, l);
            return hsl;
        }
    }
    test(){
        let direction = directions[Math.floor(Math.random() * directions.length)]
        print(direction);
    }
    walk() {
        let direction = directions[floor(random() * directions.length)];
        switch (direction) {
            case 'top':
                this.x += 0;
                this.y = this.yConstrain(this.y - this.step);
                break;
            case 'right':
                this.x = this.xConstrain(this.x + this.step);
                this.y += 0;
                break;
            case 'down':
                this.x += 0;
                this.y = this.yConstrain(this.y + this.step);
                break;
            case 'left':
                this.x = this.xConstrain(this.x - this.step);
                this.y += 0;
                break;
            case 'top-right':
                this.x = this.xConstrain(this.x + this.step);
                this.y = this.yConstrain(this.y - this.step);
                break;
            case 'down-right':
                this.x = this.xConstrain(this.x + this.step);
                this.y = this.yConstrain(this.y + this.step);
                break;
            case 'down-left':
                this.x = this.xConstrain(this.x - this.step);
                this.y = this.yConstrain(this.y + this.step);
                break;
            case 'top-left':
                this.x = this.xConstrain(this.x - this.step);
                this.y = this.yConstrain(this.y - this.step);
                break;
        }
    }
    xConstrain(num) {
        const xMIN = 0 + this.size;
        const xMAX = width - this.size;
        return Math.min(Math.max(num, xMIN), xMAX);
    }
    yConstrain(num) {
        const yMIN = 0 + this.size;
        const yMAX = height - this.size;
        return Math.min(Math.max(num, yMIN), yMAX);
    }

}
