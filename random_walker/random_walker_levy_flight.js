let walker;
var directions = ['left', 'stay', 'down-right', 'top', 'down-left', 'right', 'top-right', 'down', 'top-left'];

function setup() {
    createCanvas(450, 900);
    // frameRate()
    background(250);
    colorMode(HSB);
    walker = new RandomWalker(4);
}

function draw() {
    walker.render();
    walker.walk();
}
class RandomWalker {
    constructor(size) {
        this.pos = createVector(width / 2, height / 2);
        this.prevPos = this.pos.copy();
        this.size = size;
        this.step = this.size;
        this.c = this.pickColor();
    }
    pickColor() {
        let h = random(360);
        let s = random(45, 85);
        let l = random(35, 66);
        let a = 0.35;
        let hsla = color(h, s, l, a);
        return hsla;
    }
    render() {
        stroke(this.c);
        strokeWeight(this.size)
        line(this.pos.x, this.pos.y, this.prevPos.x, this.prevPos.y);

    }
    walk() {
        this.prevPos.set(this.pos);
        let dir = p5.Vector.random2D().mult(this.step);
        let prob = random(1);

        if (prob < 0.01) {
            dir = p5.Vector.random2D().mult(random(this.size * 5, this.size * 35));
        }
        this.pos.add(dir);

        this.pos.x = constrain(this.pos.x, 0 + (this.size / 2), width - (this.size / 2));
        this.pos.y = constrain(this.pos.y, 0 + (this.size / 2), height - (this.size / 2));
    }
}
