let stars = [];
let w, h;
let starsCount = 2000;
let rotary = 1,
    rotarySpeed = 0.00025;
let upRotation = true;

function spin(rotary_) {
    let i = floor(random(100));

    if (i == 1 && upRotation) {
        rotary -= rotarySpeed;
        !upRotation;
    } else if (i == 99 && !upRotation) {
        rotary += rotarySpeed;
        !upRotation;
    } else {
        rotary += rotarySpeed;
        upRotation = true;
    }

    rotate(rotary_);
}

function setup() {
    w = windowWidth;
    h = windowHeight;

    createCanvas(w, h);
    colorMode(HSB, 255);

    for (let i = 0; i < starsCount; i++) {
        stars[i] = new Star(100);

    }
}

function draw() {
    background(0);

    push()
    translate(width / 2, height / 2);
    spin(rotary);
    stars.forEach(star => {
        star.update();
        star.render();
    });
    pop();
}

class Star {
    constructor(maxSpeed) {
        this.maxSpeed = maxSpeed;
        this.speed = 1;
        this.c = color(125, 255, 255, 190);

        this.x = random(-width, width);
        this.y = random(-height, height);
        this.z1 = random(width);
        this.z2 = this.z1;

    }

    update = function () {
        this.speed = map(mouseY, 0, height, 1, this.maxSpeed);
        this.z1 -= this.speed;
        if (this.z1 < 1) {
            this.z1 = width;
            this.x = random(-width, width);
            this.y = random(-height, height);
            this.z2 = this.z1;
        }
    }

    render = function () {
        this.x1 = map(this.x / this.z1, 0, 1, 0, width);
        this.y1 = map(this.y / this.z1, 0, 1, 0, height);

        this.x2 = map(this.x / this.z2, 0, 1, 0, width);
        this.y2 = map(this.y / this.z2, 0, 1, 0, height);
        this.z2 = this.z1;

        stroke(this.c);
        line(this.x2, this.y2, this.x1, this.y1);
    }
}
